// const array1 = [1, 30, 4, 21, 100000]
// array1.sort((a, b) => {
//     return a - b

// })
// console.log(array1)

let items = [
    { name: 'Daniel', value: 30 },
    { name: 'Sharpe', value: 25 },
    { name: 'Kim', value: -12 },
    { name: 'Test', value: 20 }
]

items.sort((a, b) => {
    return a.value - b.value
})

console.log(items)