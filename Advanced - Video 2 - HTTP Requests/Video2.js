// const callback = (error, data) => {
//     if (error) {
//         console.log('>>>Calling callback with error', error)
//     }
//     if (data) {
//         console.log('>>>Calling callback with data', data)
//     }
// }

// getTodos = ((id, callback) => {
//     return new Promise((resolve, reject) => {
//         var request = new XMLHttpRequest()

//         request.onreadystatechange = function () {
//             if (this.readyState === 4 && this.status === 200) {
//                 const data = JSON.parse(request.responseText)
//                 const dataString = JSON.stringify(data)
//                 //callback(undefined, data)
//                 // callback(undefined, dataString)
//                 // callback(undefined, request.responseText)
//                 //callback()
//                 resolve(data)
//             }
//             if (this.readyState === 4 && request.status !== 200) {
//                 reject('Something Wrong with id: ', id)
//                 // callback('Something wrong', undefined)
//                 //callback()
//             }

//         }
//         request.open("GET", `https://jsonplaceholder.typicode.com/todos/${id}`, true)
//         //request.open("GET", "Advanced - Video 2 - HTTP Requests/data.json", true)
//         request.send()

//     })


// })

// fetch('https://jsonplaceholder.typicode.com/todos/1')
//     .then(Response => {
//         return Response.json()

//     }).then(data => {
//         console.log('>>>Check fetch data:', data)
//     })
// getTodos(1).then(data => {
//     console.log('>>>Get data1', data)
//     return getTodos(2)
// }).then(data2 => {
//     console.log('>>>Get data2', data2)
// })
//     .catch(err => {
//         console.log('>>> Error:', err)
//     })

// getTodos(1).then(data => {
//     console.log('>>>Get data', data)
// }).catch(err => {
//     console.log('>>> Error:', err)
// })

// function getTodos(callback) {
//     var request = new XMLHttpRequest()

//     request.onreadystatechange = function () {
//         if (this.readyState === 4 && this.status === 200) {
//             const data = request.responseText
//             callback(undefined, data)
//         }
//         if (this.readyState === 4 && request.status !== 200) {
//             callback('Something wrong', undefined)
//         }

//     }

//     request.open("GET", "https://jsonplaceholder.typicode.com/todos", true)
//     request.send()
// }


// getTodos(1, callback)
// getTodos(2, callback)


// const promiseExp = () => {
//     // return new Promise(function (resolve, reject) {

//     // })
//     return new Promise((resolve, reject) => {
//         resolve({ name: 'Daniel', age: '30' })
//         reject('Something wrong')
//     })
// }

// promiseExp().then(data => {
//     console.log(data)
// })
//     .catch(error => {
//         console.log(error)
//     })


// const getNewToDo = async (id) => {
//     let response = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`)

//     if (response && response.status !== 200) {
//         throw new Error('Something Wrong with status code: ' + response.status)
//     }
//     let data = await response.json()
//     return data
// }

// const getNewToDo = async (id) => {
//     try {
//         let response = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`)

//         if (response && response.status !== 200) {
//             throw new Error('Something Wrong with status code: ' + response.status)
//         }
//         let data = await response.json()
//         return data
//     } catch (e) {
//         console.log('Check Catch error: ', e)
//     }

// }


// getNewToDo('12312').then(data => {
//     console.log('>>>Check get data: ', data)
// })
//     .catch(err => {
//         console.log('>>>Check error: ', err.message)
//     })


//...
let arr1 = [1, 2, 3, 4, 5]
let state = {
    name: 'Daniel',
    address: 'BN',
    age: '30'
}
console.log('>>Check data arra1: ', arr1)

let arr2 = { ...state, age: '30' }
console.log('>>Check data arra1: ', arr2)

let { name, age, address } = state