

//Fitler and Find

//let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

let arr = [
    { name: 'Eric', age: 25 },
    { name: 'Daniel', age: 30 },
    { name: 'Kim', age: 29 },
    { name: 'Monica', age: 25 },
    { name: 'DW', age: 100 },
]

let filter = arr.filter((item, index) => {
    console.log('>>>Check filter: ', item, 'Index:', index)
    return item && item.age === 25
})

console.log(filter)

let find = arr.find((item) => {
    return item && item.age === 25
})

console.log(find)